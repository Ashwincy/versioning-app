
#reference : https://section411.com/2019/07/hello-world/
resource "aws_iam_role" "api-task-execution-role" {
  name               = "api-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs-task-assume-role.json
}

data "aws_iam_policy_document" "ecs-task-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "ecs-task-execution-role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role" {
  role       = aws_iam_role.api-task-execution-role.name
  policy_arn = data.aws_iam_policy.ecs-task-execution-role.arn
}


# create the ECS cluster
resource "aws_ecs_cluster" "application-ecs-cluster" {
  name = "version-display-app"

  tags = {
    Name = "version-disaplay-app"
  }
}

# create and define the container task
resource "aws_ecs_task_definition" "application-ecs-task" {
  family = "version-display-app"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn  = aws_iam_role.api-task-execution-role.arn
  cpu = 512
  memory = 2048
  container_definitions = <<DEFINITION
[
   {
      "name":"version-display-app",
      "image":"${var.version_app_image}",
      "essential":true,
      "portMappings":[
         {
            "containerPort": 8086,
            "hostPort": 8086,
            "protocol":"tcp"
         }
      ]
   }
]
DEFINITION
}


resource "aws_ecs_service" "version-display-service" {
  name = "version-disaply-service"
  cluster = aws_ecs_cluster.application-ecs-cluster.id
  task_definition = aws_ecs_task_definition.application-ecs-task.arn
  desired_count = 2
  launch_type = "FARGATE"

  network_configuration {
    security_groups = var.security_groups
    subnets = var.application_public_subnet
    assign_public_ip = true
  }

  load_balancer {
    container_name = "version-display-app"
    container_port = var.version_app_port
    target_group_arn = var.target_group_arn
  }

  depends_on = [
    var.app-alb-listener
  ]

lifecycle {
    create_before_destroy = true
  }
}