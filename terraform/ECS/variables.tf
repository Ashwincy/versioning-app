variable "version_app_image" {
  default = "737523751213.dkr.ecr.eu-central-1.amazonaws.com/versioncheck:latest"
}

variable "version_app_port" {
  description = "Port exposed by the application"
  default = 8086
}

variable "vpc_id" {}

variable "app-alb-listener" {}

variable "security_groups" {}

variable "target_group_arn" {}

variable "application_public_subnet" {}

