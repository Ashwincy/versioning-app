# create a security group to access the ECS application
resource "aws_security_group" "app-alb-sg" {
  name = "app-alb"
  description = "access to the application load balancer"
  vpc_id = var.vpc_id
  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# create security group to access the ECS cluster 
resource "aws_security_group" "app-ecs-sg" {
  name = "app-ecs-from-alb"
  description = "control access to the ecs cluster"
  vpc_id = var.vpc_id
  ingress {
    from_port = 8086
    protocol = "TCP"
    to_port = 8086
    security_groups = [aws_security_group.app-alb-sg.id]
  }

  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# create the ALB
resource "aws_alb" "app-alb" {
  load_balancer_type = "application"
  name = "app-alb"
  subnets = var.application_public_subnet
  security_groups = [aws_security_group.app-alb-sg.id]
}

# point redirected traffic to the app
resource "aws_alb_target_group" "app-target-group" {
  name = "app-ecs-target-group"
  port = 80
  protocol = "HTTP"
  vpc_id = var.vpc_id
  target_type = "ip"
}

# direct traffic through the ALB
resource "aws_alb_listener" "app-alb-listener" {
  load_balancer_arn = aws_alb.app-alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.app-target-group.arn
    type = "forward"
  }
}

#route 53 assign alb to a domain
resource "aws_route53_zone" "primary" {
  name = "ashwin-cy.de"
}
resource "aws_route53_record" "app" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "app.ashwin-cy.de"
  type    = "A"

  alias {
    name  = aws_alb.app-alb.dns_name
    zone_id = aws_alb.app-alb.zone_id
    evaluate_target_health = false
  }
}
