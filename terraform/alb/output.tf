output "alb-dns-name" {
  value = aws_alb.app-alb.dns_name
}

output "app-ecs-sg" {
  value = aws_security_group.app-ecs-sg.*.id
}

output "app-target-group" {
    value = aws_alb_target_group.app-target-group.id
}

output "app-alb-listener" {
    value = aws_alb_listener.app-alb-listener
}