provider "aws" {
  profile = "default"
  region = "eu-central-1"
}

module "network" {
  source = "./network"
}

module "ecs" {
  source = "./ECS"
  vpc_id = "${module.network.vpc_id}"
  security_groups =  "${module.alb.app-ecs-sg}"
  target_group_arn = "${module.alb.app-target-group}"
  app-alb-listener =  "${module.alb.app-alb-listener}"
  application_public_subnet = "${module.network.public_subnet}"    
}


module "alb" {
    source = "./alb"
    application_subnet = "${module.network.application-sg}"
    vpc_id = "${module.network.vpc_id}"
    application_public_subnet = "${module.network.public_subnet}"    
}