output "vpc_id" {
    value = aws_vpc.application-vpc.id
}

output "public_subnet" {
    value = aws_subnet.application-public-subnet.*.id
}

output "private_subnet" {
    value = aws_subnet.application-private-subnet.*.id
}

output "application-sg" {
    value = aws_security_group.application-public-sg.id
}