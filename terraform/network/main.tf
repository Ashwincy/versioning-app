

data "aws_availability_zones" "azs" {}

resource "aws_vpc" "application-vpc" {
  cidr_block            = var.vpc_cidr
  enable_dns_hostnames  = true
  enable_dns_support    = true

  tags = {
    Name = "application-vpc"
  }
}

resource "aws_internet_gateway" "application-gateway" {
  vpc_id = aws_vpc.application-vpc.id

  tags = {
    Name = "application-gateway"
  }
}

# create a Route Table for the VPC
resource "aws_route_table" "application-routing-public" {
  vpc_id = aws_vpc.application-vpc.id

  route {
    cidr_block = var.wide_route
    gateway_id = aws_internet_gateway.application-gateway.id
  }

  tags = {
    Name = "application-routing-public"
  }
}

resource "aws_default_route_table" "application-private-default" {
  default_route_table_id = aws_vpc.application-vpc.default_route_table_id

  tags = {
    Name = "application-private-default"
  }
}

resource "aws_subnet" "application-public-subnet" {
  count = 2
  cidr_block = var.public_cidrs[count.index]
  map_public_ip_on_launch = true
  vpc_id = aws_vpc.application-vpc.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]

  tags = {
    Name = "application-public-subnet-${count.index + 1}"

  }

}

resource "aws_subnet" "application-private-subnet" {
  count = 2
  cidr_block        = var.private_cidrs[count.index]
  vpc_id            = aws_vpc.application-vpc.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]
  tags = {
    Name = "application-private-subnet-${count.index + 1}"
  }
}


# associate the public subnets with the public route table
resource "aws_route_table_association" "application-public-route-assc" {
  count = 2
  route_table_id = aws_route_table.application-routing-public.id
  subnet_id = aws_subnet.application-public-subnet.*.id[count.index]
}

# create security group
resource "aws_security_group" "application-public-sg" {
  name = "application-public-group"
  description = "access to public instances"
  vpc_id = aws_vpc.application-vpc.id
}


