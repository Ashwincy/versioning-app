variable "wide_route" {
  default     = "0.0.0.0/0"
}

variable "public_cidrs" {
  description = "Public Subnet  Block"
   default = [
    "10.0.1.0/24",
    "10.0.2.0/24"
  ]
}

variable "private_cidrs" {
  description = "Private Subnet Block"
    default = [
    "10.0.3.0/24",
    "10.0.4.0/24"
  ]
}

variable "vpc_cidr" {
  default     = "10.0.0.0/16"
}