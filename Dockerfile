FROM node

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY app/ ./

RUN npm install

EXPOSE 8086
CMD [ "npm", "start" ]

