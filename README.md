# versioning-app

This code base contains
1. app folder with simple REST API written in NodeJS
2. terraform folder with terrafrom modules to deploy the app in the ECS cluster
3. output of the app is hosted on app.ashwin-cy.de home page and app.ashwin-cy.de/version for the version display using route53

what could be improved to make it suitable for the Production
1. implement CI/CD so when app is changed new docker version is built and push image to the ECR repository
2. create different stages like test, shadow and production to make sure testing is done on deployment before pushing it to prod
3. add cloudwatch feature and autoscaling based on performance metrices so scale out and scale in is taken care automatically
   also alerting based on resource usages
4. add tls certficates to the domain 
5. create a proper architectural design with components and dependencies
6. terraform state file back up for example using s3 buckets
