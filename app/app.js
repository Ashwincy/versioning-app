var express = require('express');
var app = express();
var version = '1.0';

app.get('/', function (req, res) {
      res.send('welcome to the home page');
})


app.get('/version', function (req, res) {
      console.log( version );
      res.send( version );
})

var server = app.listen(8086, function () {
   var port = server.address().port
   console.log("application path is http://localhost:%s/version", port)
})
